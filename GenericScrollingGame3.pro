#-------------------------------------------------
#
# Project created by QtCreator 2014-06-10T11:33:54
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = GenericScrollingGame3
TEMPLATE = app


SOURCES += main.cpp\
        dialog.cpp \
    play.cpp

HEADERS  += dialog.h \
    play.h

FORMS    += dialog.ui

RESOURCES += \
    images.qrc
