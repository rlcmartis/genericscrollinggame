#include "dialog.h"
#include "ui_dialog.h"
#include <QtDebug>
#include <cstdlib>
#include <cmath>
#include <QDialogButtonBox>

Dialog::Dialog(QWidget *parent)
    : QDialog(parent)/*,ui(new Ui::Dialog)*/
{
//    ui->setupUi(this);
    x1 = 0;
    x2 = 600;
    x_cone = 200;
    y_cone = 250;
    y_car =80;
    conta = 0;
    resize(600, 400);
    myTimer = new QTimer(this);
    connect(myTimer, SIGNAL(timeout()), this, SLOT(mySlot()));
    myTimer->start(1);

    //meh = new QGridLayout;

   // playButton = new QPushButton("play");
    retry = new QPushButton("retry");
//    meh->addWidget(playButton);
//    playButton->setMinimumSize(10,10);

//    this->setLayout(meh);
  // playButton->show();

  //  QObject::connect(ui->playbu, SIGNAL(clicked()),this,SLOT(on_playbu_clicked()));


}

Dialog::~Dialog()
{
    delete playButton;
    delete myTimer;
    delete meh;
    delete ui;
}


void Dialog::paintEvent(QPaintEvent *event){
    QPainter p(this);
    QString racer = ":/images/racer.png";
    QString road = ":/images/road.png";
    QString road2 = ":/images/road2.png";
    QString cone = ":/images/cone.png";

    if((y_cone >= y_car-20 && y_cone <= y_car+20) && x_cone == 55){
        collision();
    }

    p.drawPixmap(x1,0,600,400,QPixmap(road));
    p.drawPixmap(x2,0,600,400,QPixmap(road2));


    x1 = x1-1;
    x2 = x2-1;
    x_cone = x_cone -1;

    //cones part
    p.drawPixmap(x_cone,y_cone,50,50,QPixmap(cone));

    p.drawPixmap(10,y_car,80,50,QPixmap(racer));
    if (x1 < -600){
        x1=599;
        y_cone = ((rand() % 12)+1)*20;
        x_cone = (rand() % 100)+525;
    }
    if (x2 < -600){
        x2=599;
        y_cone = ((rand() % 12)+1)*20;
        x_cone = (rand() % 100)+525;
    }
}

void Dialog::keyPressEvent(QKeyEvent *event){
    qDebug() << "qwe";

        if((event->key() == Qt::Key_Up) && y_car > 80){
            y_car = y_car - 20;
        }

        if((event->key() == Qt::Key_Down) && y_car < 280){
            y_car = y_car + 20;
        }
        repaint();

}

void Dialog::mySlot(){
    repaint();
}

void Dialog::collision(){
    qDebug() << "Collision!!";
    myTimer->stop();
    retry->show();
    QObject::connect(retry, SIGNAL(clicked()), this, SLOT(newGame()));

}

void Dialog::play(){
    qDebug()<< "playing";
    myTimer->start(1);
    connect(myTimer, SIGNAL(timeout()), this, SLOT(mySlot()));

}

void Dialog::newGame(){
    x1 = 0;
    x2 = 600;
    x_cone = 200;
    y_cone = 250;
    y_car =80;
    resize(600, 400);
    myTimer = new QTimer(this);
    connect(myTimer, SIGNAL(timeout()), this, SLOT(mySlot()));
    myTimer->start(1);
    retry->close();
}


void Dialog::on_playbu_clicked()
{
  //  play();
 //   ui->playbu->close();
    //playbu.close();
}
