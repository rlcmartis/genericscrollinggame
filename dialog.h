#ifndef DIALOG_H
#define DIALOG_H
#include <QTimer>
#include <QDialog>
#include <QtGui>
#include <QtCore>
#include <QGridLayout>
#include <QPushButton>

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    int x1;
    int x2;
    int x_cone;
    int y_cone;
    int y_car;
    int conta;
    void collision();

    ~Dialog();

public slots:
    void mySlot();
    void play();
    void newGame();

protected:
    Ui::Dialog *ui;
    void paintEvent(QPaintEvent *event);
    void keyPressEvent(QKeyEvent * event);
    QTimer *myTimer;
    QWidget *wdg;
    QGridLayout *lay;
    QPushButton *playButton;
    QPushButton *retry;
    QGridLayout *meh;
private slots:

    void on_playbu_clicked();
};

#endif // DIALOG_H
